

## HP93000 Shmoo Excel Tools

HP93000 Shmoo Excel Tools read the shmoo files in hp93000 shmoo text-file format, and show them or overlay them.

PS. The source code repository on BitBucket will be migrated to Git and get no more update as [BitBucket drops Mercurial support](https://bitbucket.org/blog/sunsetting-mercurial-support-in-bitbucket "Sunsetting Mercurial support in Bitbucket"). The project is also migrated to SourceForge - [HP93kShmooExcel](https://sourceforge.net/projects/hp93kshmooexcel) for future developments.

### Build a Macro-Enabled Workbook with HP93000 Shmoo Excel Tools

The codes are exported by *[vbaDeveloper](https://github.com/hilkoc/vbaDeveloper "vbaDeveloper")*. They can be imported by vbaDeveloper directly to build a workbook with all necessary macros theoretically. If some modules or  class modules are not imported, drag them to the Excel VBA project manually. And then add *Microsoft Scripting Runtime* in *Tools > References* to enable Dictionary object support.  


If *vbaDeveloper* is not used, just follow steps below:

1. Open a new workbook 
2. Press `Alt+F11` to open VBA editor 
3. In VBA project of this workbook
	1. Select *Microsoft Excel Objects*
	2. Double-click `ThisWorkbook`
	3. Fill it with all the codes in the file *ThisWorkbook.sheet.cls*.
4. Drag all other *.bas and *.cls into the same project.
5. In *Editor menu > Tools > References*, add *Microsoft Scripting Runtime*.
6. Save it as Macro-Enabled Workbook (.xlsm) or Excel Add-In (.xlam)


After re-opening the saved workbook, there will be a new entry `HP93kShmooExcel` in Add-Ins in Excel menu.

The VBScript '00-create_xlsm.vbs' can create a .xlsm file and save the works in steps 1--4 and leave step 5 for users to finish (The Excel option *Trust Center > Macro Settings > 'Trust access to the VBA project object model'* may need to be enabled). Or it only creates a .xlsm with no macro at all. Sometimes the VBScript may create a corrupted .xlsm file in some computers (I don't know why :-/ ). Then please forget the .vbs script.


### Functions

The major functions of the program are to show raw shmoos and overlay these raw shmoos. Besides, users can define the color map. The 3 major functions are put in 3 sub menus in `HP93kShmooExcel`: `Raw Shmoo`, `Overlay Shmoo`, and `Color Map` respectively.

#### Show Raw Shmoos

The raw shmoos are originally in text format, like this:

	    hp93000,shmoo,0.1
	    SP2D&
	    CT2D*tAC vs tCK&
	    RP2D*@&
	    XP2D*TAC*(DQ)**tCK&
	    YP2D*TPhase*(@)**tCK&
	    XV2D*0*4*#11&
	    YV2D*0*0.4*#9&
	    DX2D*FFFFFFFFFFF&
	    DX2D*FFFFFFFFFFF&
	    DX2D*FFFFFFFFFFF&
	    DX2D*FFFFFFFFFFF&
	    DX2D*PPPPPPPFFFF&
	    DX2D*PPPPPPFFFFF&
	    DX2D*PPPPPPFFFFF&
	    DX2D*PPPPPPFFFFF&
	    DX2D*PPPPPFFFFFF&
	    ED2D&

The program will read the files, store them in a _raw shmoo sheet_, and colorize the shmoo.

The available operations in menu *HP93kShmooExcel > Raw Shmoo*:

* Create a new one: read the hp93000 shmoo files, and create a new workbook with a raw shmoo sheet. 
* Add new 93k shmoo: in a raw shmoo sheet or totally empty worksheet, insert new raw shmoos from hp93000 shmoo files.
* New color map: not implemented yet.
* Recreate 'Select all' link: sometimes, the links to raw shmoos become invalid. This function can recreate the links.

The program only support simple pass/fail shmoos and overlay shmoos by hp93000 shmoo plot program. Other kinds of shmoos, such as fail bit count, or the first fail, are not supported.

The original file name of a raw shmoo is stored to be an identification. Thus the original file name will be unique inside a raw shmoo sheet. The cell storing the original file name is a hyperlink to select its shmoo plot. 
     

#### Overlay of Raw Shmoos

The program can create an overlay shmoo from raw shmoos, add more raw shmoos, select only some raw shmoos and colorize the overlay shmoo.

The available operations in menu *HP93kShmooExcel > Overlay Shmoo*:

* Create overlay shmoo sheet _OVL_: All raw shmoos in a sheet will be merged into result overlay shmoo in a sheet named _OVL_. The sheet _OVL_ has a list containing all entries in the raw shmoo sheet. Each entries are recorded with its raw shmoo sheet and the original file name. All entries are initially enabled. You can rename _OVL_ so as to create other overlay shmoo sheet.  
* Add more raw shmoos: add more raw shmoos into overlay shmoo sheet. To overlay raw shmoos from multiple raw shmoo sheets can be achieved by this function.
* Re-run overlay: use it when re-running the overlay is needed, such as changing the selection of raw shmoos, or new color map.     


2 kinds of overlays:  

* _normal overlay_: sum up all specified raw shmoos and calculate the failure rate of each point in shmoo. Put any characters, such as numbers or letters, in the _Add?_ field to add the raw shmoo into result overlay shmoo, except the  _differential_ case described below.   
* _differential_: when exact one `A` and one `B` are specified in _Add?_ field, the overlay shmoo will be a differential shmoo.

Overlay shmoos are restricted to resident in the same workbook as raw shmoos. Or the program cannot find raw shmoos. The names of raw shmoo sheets names also need cares if you want to change the sheet name. 


#### Use a Color Map

The program uses a default color map with green as 0 and gradient to red as max(>0).
New color map can be created and defined. After defining a new color map, re-running the overlay is needed.


A color map is Excel cells in *n* rows x *1* column. Each cell contains a numerical value and its associated fill-color. A string "gradient" is also allowed to indicate 2-color-scale gradient or 3-color-scale gradient. Other values are ignored.


3 kinds of color maps:

* 2 color scale gradient: defining the color of minimum and maximum. It uses Excel built-in 2-color-scale conditional formatting.
* 3 color scale gradient: defining the color of minimum, middle, and maximum. It uses Excel built-in 3-color-scale conditional formatting.
* multiple levels: defining values with associated colors. When the cell value is greater or equal to a value, the cell will have the associated color. Unlike 2-color-scale or 3-color-scale, the colors of multiple levels are not gradient because Excel does not support gradient for color count > 3 in conditional formatting.  

The color maps can be in workbook-scope or sheet-scope. The priority: sheet-scope > workbook-scope > default.


The available operations in menu *HP93kShmooExcel > Color Map*:

* Define color map: define a workbook-scope or sheet-scope color map according the range selected
* Remove workbook color map
* Remove sheet color map: go to the worksheet with color map to be removed and then run this function.
* Dump example color map: dump some example color maps. Start in a totally empty worksheet to run this function.

A example flow to use color map for the first raw shmoo sheet:

1. Open new empty workbook
2. Dump example color, define new color map, or copy color map from an existent workbook.
3. add an empty worksheet
4. add 93000 new raw sheets in the empty worksheet (*Raw Shmoo* > *Add new 93k shmoo*) 


### How the Shmoos are Displayed

HP93000 Shmoo Excel Tools show the shmoos in cells in Excel worksheets. The program does not draw, use any graphical functions, or use Excel plots at all. All "plots" are done by putting proper characters in cells and adjusting their formats. The "plots" can then be selected and copied as pictures via Excel _Copy as Picture_ function later.


[//]: <> (__END__)

