Attribute VB_Name = "module_Shmoo"
'Option Private Module

Function import_shmfile(filetoopen) As Variant

    Dim K
    Dim f, fs
    Dim TextLine
    Dim ts
    Set f = CreateObject("Scripting.FileSystemObject").GetFile(filetoopen)
    Set ts = f.OpenAsTextStream(1, 0)
    
    Dim aDat()
    Dim n
       
    ReDim aDat(1 To 256)
    n = 1
    Do
        If n > UBound(aDat) Then ReDim Preserve aDat(1 To n + 255)
        aDat(n) = ts.ReadLine
        n = n + 1
    Loop Until ts.AtEndOfStream
    ts.Close
    
    If n > 1 Then ReDim Preserve aDat(1 To n - 1)
    
    import_shmfile = aDat
    
End Function

Sub generate_overlay_shmoo2(sht As Worksheet, clct_source_shmoo As Collection)

    Dim t
    Dim n_x
    Dim n_y
    Dim sht_source As Worksheet
    Dim obj_source As CRawShmooSheet
    Dim bk_overlay As Workbook
    Set bk_overlay = sht.Parent
    Set obj_source = New CRawShmooSheet
    
    Dim f_sheet_empty As Boolean

    f_sheet_empty = sht.UsedRange.Address = "$A$1" And WorksheetFunction.CountA(sht.UsedRange) = 0
    
    If clct_source_shmoo.Count = 0 Then Exit Sub
    t = clct_source_shmoo(1)
    Set sht_source = bk_overlay.Sheets(t(0))
    
    obj_source.Sheet = sht_source
    If Not obj_source.IsRawShmooSheet Then Exit Sub
    
    If f_sheet_empty Then
        With sht
            .Cells(1, 1) = "Overlay shmoos"
            .Cells(2, 1) = "Title"
            .Cells(3, 1) = "Result Pin"
            .Cells(4, 1) = "X-Parameter"
            .Cells(5, 1) = "Y-Parameter"
            .Cells(6, 1) = "X-Value"
            .Cells(7, 1) = "Y-Value"
            .Cells(10, 1) = "Run-count"
            .Cells(11, 1).Resize(1, 4) = Array("Add?", "Status", "Sheet", "Original file")
        End With
        
        With sht.Cells.Font
            .Name = "Lucida Console"
            .Size = 9
        End With
        
        With obj_source
            sht.Cells(2, 2) = .Title
            sht.Cells(3, 2) = .ResultPin
            sht.Cells(4, 2) = .XParameter
            sht.Cells(5, 2) = .YParameter
            sht.Cells(6, 2).Resize(, 4).Value = .XValues
            sht.Cells(7, 2).Resize(, 4).Value = .YValues
        End With
        
        gsx = Evaluate(sht_source.Names("gsx").Value)
        gsy = Evaluate(sht_source.Names("gsy").Value)
        
        Call sht.Names.Add("gsx", gsx)
        Call sht.Names.Add("gsy", gsy)
    Else
        If sht.Cells(1, 1) <> "Overlay shmoos" Then Exit Sub
    End If
    
    n_x = sht.Cells(6, 2).Offset(0, 3).Value
    n_y = sht.Cells(7, 2).Offset(0, 3).Value
    
    gsx = Evaluate(sht.Names("gsx").Value)
    gsy = Evaluate(sht.Names("gsy").Value)
    
    
    Dim i As Integer
    Dim rng_list_source As Range
    Set rng_list_source = sht.Cells(11, 1)
    
    Dim dict_raw_shmoo As Dictionary
    
    '--- add new entries in source list
    i = 1
    Set dict_raw_shmoo = New Dictionary
    With rng_list_source
        Do Until .Offset(i, 2).Value & "" = ""
            If Not dict_raw_shmoo.Exists(.Offset(i, 2).Value) Then
                Set dict_raw_shmoo(.Offset(i, 2).Value) = New Dictionary
            End If
            dict_raw_shmoo(.Offset(i, 2).Value).Item(.Offset(i, 3).Value) = 1
            i = i + 1
        Loop
                
        For Each t In clct_source_shmoo
            If Not dict_raw_shmoo.Exists(t(0)) Then
                Set dict_raw_shmoo(t(0)) = New Dictionary
            End If
            If Not dict_raw_shmoo(t(0)).Exists(t(1)) Then
                dict_raw_shmoo(t(0)).Item(t(1)) = 1
                .Offset(i, 0) = "Y"
                .Offset(i, 2) = t(0)
                .Offset(i, 3) = t(1)
                i = i + 1
            End If
        Next
    End With
    
    '--- check if use differential shmoo:
    '---   only one 'A' and only one 'B', each one is run-count=1 non-overlay shmoo
    Dim myIsDifferential As Boolean
    Dim iShmooA
    Dim iShmooB
    myIsDifferential = True
    i = 1
    iShmooA = 0
    iShmooB = 0
    With rng_list_source
        Do Until .Offset(i, 2).Value & "" = "" Or myIsDifferential = False
            If .Offset(i, 0).Value & "" = "" Then
                ' skip
            ElseIf .Offset(i, 0).Value = "A" Then
                If iShmooA <> 0 Then myIsDifferential = False
                iShmooA = i
            ElseIf .Offset(i, 0).Value = "B" Then
                If iShmooB <> 0 Then myIsDifferential = False
                iShmooB = i
            Else
                myIsDifferential = False
            End If
            i = i + 1
        Loop
        If myIsDifferential Then
            If iShmooA = 0 Or iShmooB = 0 Then
                myIsDifferential = False
            Else
                obj_source.Sheet = bk_overlay.Sheets(.Offset(iShmooA, 2).Value)
                If obj_source.RunCount(.Offset(iShmooA, 3).Value) <> 1 Then myIsDifferential = False
                obj_source.Sheet = bk_overlay.Sheets(.Offset(iShmooB, 2).Value)
                If obj_source.RunCount(.Offset(iShmooB, 3).Value) <> 1 Then myIsDifferential = False
            End If
        End If
    End With
        
    '--- get overlay according to source list
    Dim rng_result_shmoo_plot As Range
    Set rng_result_shmoo_plot = sht.Cells(11, 1).Offset(4, 10).Resize(n_y * gsy, n_x * gsx)
    
    i = 1
    sht.Cells(10, 2) = 0
    rng_result_shmoo_plot.ClearContents
    Set dict_raw_shmoo = New Dictionary
    With rng_list_source
        Do Until .Offset(i, 2).Value & "" = ""
        
            If dict_raw_shmoo.Exists(.Offset(i, 2).Value) Then
                Set obj_source = dict_raw_shmoo(.Offset(i, 2).Value)
            Else
                Set sht_source = bk_overlay.Sheets(.Offset(i, 2).Value)
                obj_source.Sheet = sht_source
                Set dict_raw_shmoo(.Offset(i, 2).Value) = obj_source
            End If
            
            Dim run_count
            run_count = obj_source.RunCount(.Offset(i, 3).Value)
            
            If .Offset(i, 0).Value & "" = "" Then
                ' skip
                .Offset(i, 1).Value = "Skip"
            ElseIf run_count <= 0 Then
                .Offset(i, 1).Value = obj_source.ErrMsg
                
            ElseIf n_x <> obj_source.nX Or n_y <> obj_source.nY Then
                .Offset(i, 1).Value = "Diff XY-size"
                
            Else
            
                ' add run-count
                sht.Cells(10, 2) = sht.Cells(10, 2) + run_count
                
                obj_source.ShmooPlotArea(.Offset(i, 3).Value).Copy
                rng_result_shmoo_plot.PasteSpecial xlPasteValues, xlPasteSpecialOperationAdd
                If myIsDifferential And i = iShmooB Then
                    ' add additional once for shmoo B
                    rng_result_shmoo_plot.PasteSpecial xlPasteValues, xlPasteSpecialOperationAdd
                    sht.Cells(10, 2) = sht.Cells(10, 2) + run_count
                End If
                Application.CutCopyMode = False
                
                .Offset(i, 1).Value = "Added " & run_count * IIf(myIsDifferential And i = iShmooB, 2, 1)
            End If
                    
            i = i + 1
        Loop
    End With
    
    With rng_result_shmoo_plot
        .VerticalAlignment = xlCenter
        .ColumnWidth = 1
    End With
    rng_result_shmoo_plot.MergeCells = False
    sht.Cells(10, 2).Copy
    rng_result_shmoo_plot.PasteSpecial xlPasteValues, xlPasteSpecialOperationDivide, True
    Application.CutCopyMode = False
    
    Dim clct_color_map As Collection
    Set clct_color_map = Nothing
    On Error Resume Next
    Set clct_color_map = get_color_map(sht.Range("ColorMap"))
    If clct_color_map Is Nothing Then Set clct_color_map = get_color_map(bk_overlay.Names("ColorMap").RefersToRange)
    On Error GoTo 0
    
    Call FormatConditionShmooColorMap(rng_result_shmoo_plot, clct_color_map)
    
    sht.Range("A1").Select
   
    ' beautify plot area
    With rng_result_shmoo_plot
        With .Offset(-3, -4).Resize(.Rows.Count + 3 + 7, .Columns.Count + 4 + 6)
            Call SetOuterThinLine(.Offset)
            .Offset.Interior.Color = RGB(253, 233, 217)
            
            ' add hyperlink so as to select whole plot
            sht.Hyperlinks.Add Anchor:=sht.Range("G11"), _
                Address:="", SubAddress:=.Offset.Address, _
                TextToDisplay:="Select plot"

            ' restore the font after add hyperlink
            With sht.Range("G11")
                .Offset.Font.Name = .Offset(, 1).Font.Name
                .Offset.Font.Size = .Offset(, 1).Font.Size
            End With
            
            ' To truncate too long filename
            .Resize(, 1).Value = " "
        End With
        
'        Call SetOuterThinLine(.Offset)
    End With
   
   
    Dim Title
    Dim x_values, y_values
    Dim x_axis_title, y_axis_title
    With rng_shmoo
        Title = sht.Range("B2").Value
        x_values = Array(sht.Range("B6").Value, sht.Range("C6").Value)
        y_values = Array(sht.Range("B7").Value, sht.Range("C7").Value)
        x_axis_title = sht.Range("B4").Value
        y_axis_title = sht.Range("B5").Value
    End With
    Call setChartTitle(rng_result_shmoo_plot, Title)
    Call setYAxis(rng_result_shmoo_plot, y_values, y_axis_title, gsy)       ' y-axis: title, tick marks, and values, incl. Div
    Call setXAxis(rng_result_shmoo_plot, x_values, x_axis_title, gsx, gsy)  ' x-axis: title, tick marks, and values, incl. Div
    Call setShmooLegend(rng_result_shmoo_plot, myIsDifferential, clct_color_map, False)
    Call setShmooPlotRowHeightAndColumnWidth(rng_result_shmoo_plot)
    Call mergeGrains(rng_result_shmoo_plot, gsx, gsy)
    Call SetOuterThinLine(rng_result_shmoo_plot)
    
    Call HideFont(rng_result_shmoo_plot)
'    Call FormatConditionMakeInteriorColorStatic(rng_result_shmoo_plot)
'    rng_result_shmoo_plot.FormatConditions.Delete
   
   
End Sub


Private Sub FormatConditionShmoo1RunPassFail(rng As Range)
    Dim t_max
    t_max = WorksheetFunction.Max(rng)
    If t_max < 1 Then t_max = 1
    
    With rng
        .FormatConditions.Delete
        .FormatConditions.AddColorScale ColorScaleType:=3
        .FormatConditions(.FormatConditions.Count).SetFirstPriority
        
        .FormatConditions(1).ColorScaleCriteria(1).Type = xlConditionValueNumber
        .FormatConditions(1).ColorScaleCriteria(1).Value = 0
        With .FormatConditions(1).ColorScaleCriteria(1).FormatColor
            .Color = 8109667
            .TintAndShade = 0
        End With
        
        .FormatConditions(1).ColorScaleCriteria(2).Type = xlConditionValueNumber
        .FormatConditions(1).ColorScaleCriteria(2).Value = 0.5 * t_max
        With .FormatConditions(1).ColorScaleCriteria(2).FormatColor
            .Color = 8711167
            .TintAndShade = 0
        End With
        
        .FormatConditions(1).ColorScaleCriteria(3).Type = xlConditionValueNumber
        .FormatConditions(1).ColorScaleCriteria(3).Value = t_max
        With .FormatConditions(1).ColorScaleCriteria(3).FormatColor
            .Color = 7039480
            .TintAndShade = 0
        End With
    End With
End Sub

Sub FormatConditionShmooColorMap(rng As Range, Optional clct_color_map As Collection = Nothing)
    Dim t_max
    t_max = WorksheetFunction.Max(rng)
    If t_max < 1 Then t_max = 1
    
    If clct_color_map Is Nothing Then GoTo InvalidColorMap
    If clct_color_map.Count <= 1 Then GoTo InvalidColorMap
    If clct_color_map.Count = 2 And TypeName(clct_color_map.Item(clct_color_map.Count)) <> "Variant()" Then GoTo InvalidColorMap
    
    Dim i, t
    Dim nColorScale As Integer
    
    nColorScale = 0
    If TypeName(clct_color_map.Item(clct_color_map.Count)) <> "Variant()" Then
        If clct_color_map.Count - 1 = 2 Then nColorScale = 2
        If clct_color_map.Count - 1 = 3 Then nColorScale = 3
    End If
    
    rng.FormatConditions.Delete
    For Each t In clct_color_map
        If TypeName(t) <> "Variant()" Then
            ' No-OP
        ElseIf nColorScale > 0 Then
        Else
            With rng.FormatConditions.Add(xlCellValue, xlGreaterEqual, t(0) * t_max)
                .SetFirstPriority
                .Interior.Color = t(1)
                .StopIfTrue = True
            End With
        End If
    Next

    If nColorScale > 0 Then
        rng.FormatConditions.AddColorScale nColorScale
        With rng
            .FormatConditions(.FormatConditions.Count).SetFirstPriority
            
            For i = 1 To nColorScale
                t = clct_color_map.Item(i)
                .FormatConditions(1).ColorScaleCriteria(i).Type = xlConditionValueNumber
                .FormatConditions(1).ColorScaleCriteria(i).Value = t(0) * t_max
                With .FormatConditions(1).ColorScaleCriteria(i).FormatColor
                    .Color = t(1)
                    .TintAndShade = 0
                End With
            Next
        End With
    End If

    Exit Sub

InvalidColorMap:
        Call FormatConditionShmoo1RunPassFail(rng)
        
End Sub

Sub FormatConditionMakeInteriorColorStatic(rng As Range)

    Dim rng1 As Range
    
    For Each rng1 In rng
        rng1.Interior.Color = rng1.DisplayFormat.Interior.Color
    Next

End Sub

Sub HideFont(rng As Range)
    rng.NumberFormatLocal = ";;;"
End Sub

Private Function save_shmoo_parameter(line, sig_head, cell_to_save_value As Range) As Boolean
    save_shmoo_parameter = False
    t = line
    If Left(t, 5) <> sig_head Or Right(t, 1) <> "&" Then Exit Function
    cell_to_save_value.Value = Mid(t, 6, Len(t) - 6)
    save_shmoo_parameter = True
End Function

Private Function save_shmoo_xyvalues(line, sig_head, cell_to_save_value As Range, _
        f_new_empty_sheet As Boolean) As Boolean
        
    save_shmoo_xyvalues = False
    
    t = line
    If Left(t, 5) <> sig_head Or Right(t, 1) <> "&" Then Exit Function
    
    t = Split(Mid(t, 6, Len(t) - 6), "*")
    
    If f_new_empty_sheet Then
        cell_to_save_value.Offset(, 0).Value = t(0)
        cell_to_save_value.Offset(, 1).Value = t(1)
        cell_to_save_value.Offset(, 2).Value = Mid(t(2), 1, 1)
        cell_to_save_value.Offset(, 3).Value = Mid(t(2), 2) + 0
        save_shmoo_xyvalues = True
    Else
        save_shmoo_xyvalues = ( _
            cell_to_save_value.Offset(, 0).Value = t(0) + 0 And _
            cell_to_save_value.Offset(, 1).Value = t(1) + 0 And _
            cell_to_save_value.Offset(, 2).Value = Mid(t(2), 1, 1) And _
            cell_to_save_value.Offset(, 3).Value = Mid(t(2), 2) + 0 And _
            True)
    End If
    
End Function

Function get_color_map(rng As Range) As Collection
    Dim result As Collection
    Set result = New Collection
    Set get_color_map = result
    
    rng.SortSpecial order1:=xlAscending, Header:=xlNo
    
    Dim t_max, t_min
    t_max = WorksheetFunction.Max(rng)
    t_min = WorksheetFunction.Min(rng)
    If Not IsNumeric(t_max) Then Exit Function
    If t_max < 0 Then Exit Function
    If t_min >= t_max Then Exit Function
    
    Dim rng_t As Range
    Dim i
    For i = 1 To rng.Rows.Count
        Set rng_t = rng.Resize(1, 1).Offset(i - 1)
        If Not IsNumeric(rng_t.Value) Then
            If UCase(rng_t.Value) = UCase("gradient") Then
                result.Add "Gradient"
                Exit Function
            End If
        ElseIf rng_t.Value <= 0 Then
            On Error Resume Next
            result.Remove 1
            On Error GoTo 0
            result.Add Array(0, rng_t.DisplayFormat.Interior.Color)
        Else
            result.Add Array(rng_t.Value / t_max, rng_t.DisplayFormat.Interior.Color)
        End If
    Next

End Function

Private Function FileExists(fname) As Boolean
    FileExists = (Dir(fname) <> "")
End Function

Function IsEmptySheet(sht As Worksheet) As Boolean
    IsEmptySheet = sht.UsedRange.Address = "$A$1" And WorksheetFunction.CountA(sht.Range("$A$1")) = 0
End Function

Public Sub QuickSort(vArray As Variant, inLow As Long, inHi As Long)
' Code from https://stackoverflow.com/questions/152319/vba-array-sort-function
'    posted by Jorge Ferreira
'    Title: VBA array sort function?
'      via google:<excel vba sort array>

    Dim pivot   As Variant
    Dim tmpSwap As Variant
    Dim tmpLow  As Long
    Dim tmpHi   As Long
    
    tmpLow = inLow
    tmpHi = inHi
    
    pivot = vArray((inLow + inHi) \ 2)
    
    While (tmpLow <= tmpHi)
    
       While (vArray(tmpLow) < pivot And tmpLow < inHi)
          tmpLow = tmpLow + 1
       Wend
    
       While (pivot < vArray(tmpHi) And tmpHi > inLow)
          tmpHi = tmpHi - 1
       Wend
    
       If (tmpLow <= tmpHi) Then
          tmpSwap = vArray(tmpLow)
          vArray(tmpLow) = vArray(tmpHi)
          vArray(tmpHi) = tmpSwap
          tmpLow = tmpLow + 1
          tmpHi = tmpHi - 1
       End If
    
    Wend
    
    If (inLow < tmpHi) Then QuickSort vArray, inLow, tmpHi
    If (tmpLow < inHi) Then QuickSort vArray, tmpLow, inHi

End Sub

Public Sub RemoveTailEmptyRows(Optional sht As Worksheet = Nothing, Optional LR0 = 1)

    If sht Is Nothing Then Set sht = ActiveSheet

    Dim LRz
    Dim r_m
    Dim t
    Dim c
    LRz = sht.UsedRange.Row + sht.UsedRange.Rows.Count - 1
    c = sht.UsedRange.Column + sht.UsedRange.Columns.Count - 1
    Do While LRz > LR0
        r_m = Int((LR0 + LRz + 1) / 2)
        Set t = sht.Cells(LRz, c)
        
        If WorksheetFunction.CountA(sht.Range("A" & r_m & ":" & t.Address)) > 0 Then
            LR0 = r_m
        Else
            LRz = r_m - 1
        End If
    Loop
    LRz = sht.UsedRange.Row + sht.UsedRange.Rows.Count - 1
    If LRz > LR0 Then sht.Range("A" & (LR0 + 1) & ":A" & LRz).EntireRow.Delete
    t = sht.UsedRange.Address   ' refresh UsedRange
        
End Sub

'Public Sub RemoveTailEmptyColumns(Optional sht As Worksheet = Nothing, Optional LC0 = 1)
'
'    If sht Is Nothing Then Set sht = ActiveSheet
'
'    Dim LCz
'    Dim c_m
'    Dim t
'    Dim r
'    LCz = sht.UsedRange.Column + sht.UsedRange.Columns.Count - 1
'    r = sht.UsedRange.Row + sht.UsedRange.Rows.Count - 1
'    Do While LCz > LC0
'        c_m = Int((LC0 + LCz + 1) / 2)
'        Set t = sht.Cells(r, LCz)
'
'        If WorksheetFunction.CountA(sht.Range(sht.Cells(1, c_m), t)) > 0 Then
'            LC0 = c_m
'        Else
'            LCz = c_m - 1
'        End If
'    Loop
'    LCz = sht.UsedRange.Column + sht.UsedRange.Columns.Count - 1
'    If LCz > LC0 Then sht.Range(sht.Cells(1, LC0 + 1), sht.Cells(1, LCz)).EntireColumn.Delete
'    t = sht.UsedRange.Address   ' refresh UsedRange
'
'End Sub



'__END__
