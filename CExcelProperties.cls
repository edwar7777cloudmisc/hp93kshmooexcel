VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "CExcelProperties"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

'from http://codereview.stackexchange.com/questions/117037/vba-class-to-persist-and-restore-excel-application-properties
' "VBA Class to persist and restore Excel Application properties"
' by Mast
' searched by google:<excel vba restore application.screenupdating>

'These constants define the default restoration behaviors for the class
Private Const DEFAULT_RESTORE_CALCULATION = True
Private Const DEFAULT_RESTORE_DISPLAY_ALERTS = True
Private Const DEFAULT_RESTORE_ENABLE_EVENTS = True
Private Const DEFAULT_RESTORE_SCREEN_UPDATING = True

'Set this to true to ensure a persisted state is restored, even if the consumer forgets to restore
Private Const RESTORE_ON_TERMINATE = True

'Private members stored in a Type
Private this As TMembers

Private Type TMembers
  Calculation As XlCalculationState
  DisplayAlerts As Boolean
  EnableEvents As Boolean
  ScreenUpdating As Boolean
  RestoreCA As Boolean
  RestoreDA As Boolean
  RestoreEE As Boolean
  RestoreSU As Boolean
  IsPersisted As Boolean
  IsRestored As Boolean
End Type

'Set the default restoration behaviours on intialize
Private Sub Class_Initialize()
  this.RestoreCA = DEFAULT_RESTORE_CALCULATION
  this.RestoreDA = DEFAULT_RESTORE_DISPLAY_ALERTS
  this.RestoreEE = DEFAULT_RESTORE_ENABLE_EVENTS
  this.RestoreSU = DEFAULT_RESTORE_SCREEN_UPDATING
End Sub

'By default, restore the settings if we didn't do it explicitly
Private Sub Class_Terminate()
  If this.IsPersisted And Not this.IsRestored And RESTORE_ON_TERMINATE Then
    Me.Restore
  End If
End Sub

Public Property Get RestoreCalculation() As Boolean
  RestoreCalculation = this.RestoreCA
End Property

Public Property Let RestoreCalculation(Value As Boolean)
  this.RestoreCA = Value
End Property

Public Property Get RestoreDisplayAlerts() As Boolean
  RestoreDisplayAlerts = this.RestoreDA
End Property

Public Property Let RestoreDisplayAlerts(Value As Boolean)
  this.RestoreDA = Value
End Property

Public Property Get RestoreEnableEvents() As Boolean
  RestoreEnableEvents = this.RestoreEE
End Property

Public Property Let RestoreEnableEvents(Value As Boolean)
  this.RestoreEE = Value
End Property

Public Property Get RestoreScreenUpdating() As Boolean
  RestoreScreenUpdating = this.RestoreSU
End Property

Public Property Let RestoreScreenUpdating(Value As Boolean)
  this.RestoreSU = Value
End Property

Public Sub Save()
  If Not this.IsPersisted Then
    'Save all of the settings
    With Application
      this.Calculation = .Calculation
      this.DisplayAlerts = .DisplayAlerts
      this.EnableEvents = .EnableEvents
      this.ScreenUpdating = .ScreenUpdating
    End With
    this.IsPersisted = True
  Else
    Err.Raise -1000, "CExcelProperties", "Properties have already been persisted."
  End If
End Sub

Public Sub Restore()
  'Only restore the settings that we want restored
  '(which by default is all of them)
  With Application
    If this.RestoreCA Then
      .Calculation = this.Calculation
    End If
    If this.RestoreDA Then
      .DisplayAlerts = this.DisplayAlerts
    End If
    If this.RestoreEE Then
      .EnableEvents = this.EnableEvents
    End If
    If this.RestoreSU Then
      .ScreenUpdating = this.ScreenUpdating
    End If
  End With
  this.IsRestored = True
End Sub

'__END__
