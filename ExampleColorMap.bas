Attribute VB_Name = "ExampleColorMap"


Sub DumpColorMap_3ColorScaleGreenYellowRed(rng As Range)

    rng.Offset(0).Value = 0
    rng.Offset(1).Value = 0.5
    rng.Offset(2).Value = 1
    rng.Offset(3).Value = "Gradient"
    
    Call FormatConditionShmooColorMap(rng.Resize(4))

End Sub

Sub DumpColorMap_3ColorScaleBlueWhiteRed(rng As Range)

    rng.Offset(0).Value = 0
    rng.Offset(1).Value = 0.5
    rng.Offset(2).Value = 1
    rng.Offset(3).Value = "Gradient"
    
    Dim clct_color_map As Collection
    Set clct_color_map = New Collection
    
    clct_color_map.Add Array(0, 13011546)
    clct_color_map.Add Array(0.5, 16776444)
    clct_color_map.Add Array(1, 7039480)
    clct_color_map.Add "gradient"
    
    Call FormatConditionShmooColorMap(rng.Resize(4), clct_color_map)
    
    
End Sub

Sub DumpColorMap_2ColorScaleGreenRed(rng As Range)
    rng.Offset(0).Value = 0
    rng.Offset(1).Value = 1
    rng.Offset(2).Value = "Gradient"
    
    Call FormatConditionShmooColorMap(rng.Resize(3))

End Sub

Sub DumpColorMap_Simple5(rng As Range)

    With rng
        .Offset(0).Value = 0: .Offset(0).Interior.Color = RGB(0, 255, 0)
        .Offset(1).Value = 0.01: .Offset(1).Interior.Color = RGB(255, 255, 0)
        .Offset(2).Value = 1 / 3: .Offset(2).Interior.Color = RGB(0, 0, 255)
        .Offset(3).Value = 2 / 3: .Offset(3).Interior.Color = RGB(&HEF, &H82, &HEF)
        .Offset(4).Value = 0.99: .Offset(4).Interior.Color = RGB(255, 0, 0)
        .Offset(5).Value = 1: .Offset(5).Interior.Color = RGB(255, 0, 0)
    End With

End Sub

Sub DumpColorMap_Simple11(rng As Range)

    With rng
        .Offset(0).Value = 0 / 100: .Offset(0).Interior.Color = RGB(0, 255, 0)
        .Offset(1).Value = 10 / 100: .Offset(1).Interior.Color = RGB(132, 112, 255)
        .Offset(2).Value = 20 / 100: .Offset(2).Interior.Color = RGB(189, 183, 107)
        .Offset(3).Value = 30 / 100: .Offset(3).Interior.Color = RGB(165, 42, 42)
        .Offset(4).Value = 40 / 100: .Offset(4).Interior.Color = RGB(199, 21, 133)
        .Offset(5).Value = 50 / 100: .Offset(5).Interior.Color = RGB(0, 0, 255)
        .Offset(6).Value = 60 / 100: .Offset(6).Interior.Color = RGB(0, 255, 255)
        .Offset(7).Value = 70 / 100: .Offset(7).Interior.Color = RGB(255, 0, 255)
        .Offset(8).Value = 80 / 100: .Offset(8).Interior.Color = RGB(255, 192, 203)
        .Offset(9).Value = 90 / 100: .Offset(9).Interior.Color = RGB(255, 255, 0)
        .Offset(10).Value = 99 / 100: .Offset(10).Interior.Color = RGB(255, 165, 0)
        .Offset(11).Value = 100 / 100: .Offset(11).Interior.Color = RGB(255, 0, 0)
    End With

End Sub

'__END__
