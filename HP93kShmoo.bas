Attribute VB_Name = "HP93kShmoo"
Option Explicit
Option Private Module

Sub CreateRawShmooFromFiles()

    Dim cProps As CExcelProperties
    Set cProps = New CExcelProperties
    Application.ScreenUpdating = False
    
    '-----------------------------------
    Dim rawShmoo As Variant
    
    Dim filetoopen
    filetoopen = Application.GetOpenFilename("*.txt(*.txt),*.txt", , "V93000 Shmoo -- Open File", , True)
    If TypeName(filetoopen) = "Boolean" Then
        Exit Sub
    End If
    QuickSort filetoopen, LBound(filetoopen), UBound(filetoopen)
    
    Dim bk_raw_shmoo As Workbook
    Dim sht_raw_shmoo As Worksheet
    Set bk_raw_shmoo = Workbooks.Add(1)
    Set sht_raw_shmoo = bk_raw_shmoo.Worksheets(1)
    
    Dim name_sheet, t
    name_sheet = ""
    t = ""
    Do Until name_sheet <> ""
        name_sheet = Application.InputBox(t & "The name of raw shmoo worksheet. OVL-xxx is not allowed.", _
            "HP93000 Shmoo Excel Tool", "RawShmoo", , , , , 2)
            
        If name_sheet = False Then
            bk_raw_shmoo.Close False
            Exit Sub
        End If
        
        On Error Resume Next
        sht_raw_shmoo.Name = name_sheet
        On Error GoTo 0
        
        If sht_raw_shmoo.Name <> Left(name_sheet, Len(sht_raw_shmoo.Name)) Then
            t = "Invalid sheet name => " & name_sheet & vbCrLf
            name_sheet = ""
        End If
        
        If UCase(Left(sht_raw_shmoo.Name, 4)) = "OVL-" Then
            t = "Sheet name cannot begin with ""OVL-"" => " & name_sheet & vbCrLf
            name_sheet = ""
        End If
    Loop
    
    Dim obj_raw_shmoo_sheet As CRawShmooSheet
    Set obj_raw_shmoo_sheet = New CRawShmooSheet
    obj_raw_shmoo_sheet.Sheet = sht_raw_shmoo
    
    Dim filename
    For Each filename In filetoopen
        import_shmfile filename
        rawShmoo = import_shmfile(filename)
        
        obj_raw_shmoo_sheet.AddRawShmoo Dir(filename), rawShmoo
    Next
    

End Sub

Sub AddRawShmooFromFiles()

    Dim cProps As CExcelProperties
    Set cProps = New CExcelProperties
    Application.ScreenUpdating = False
    
    '-----------------------------------
    Dim sht_raw_shmoo As Worksheet
    Set sht_raw_shmoo = ActiveSheet
    
    Dim obj_raw_shmoo_sheet As CRawShmooSheet
    Set obj_raw_shmoo_sheet = New CRawShmooSheet
    obj_raw_shmoo_sheet.Sheet = sht_raw_shmoo
    
    If UCase(Left(sht_raw_shmoo.Name, 4)) = "OVL-" Or _
        UCase(sht_raw_shmoo.Name) = "OVL" Or _
        Not (obj_raw_shmoo_sheet.IsRawShmooSheet Or obj_raw_shmoo_sheet.IsEmpty) _
    Then
        MsgBox "Please begin on an empty worksheet or raw shmoo sheet. Sheet OVL-xxxx is also not allowed."
        Exit Sub
    End If
    
    If sht_raw_shmoo.Parent Is ThisWorkbook Then
        MsgBox "Please do not begin from raw shmoo sheet of this HP93kShmooExcel Workbook."
        Exit Sub
    End If
    
    Dim filetoopen
    filetoopen = Application.GetOpenFilename("*.txt(*.txt),*.txt", , "V93000 Shmoo -- Open File", , True)
    If TypeName(filetoopen) = "Boolean" Then
        Exit Sub
    End If
    QuickSort filetoopen, LBound(filetoopen), UBound(filetoopen)
    
    Dim rawShmoo As Variant
    Dim filename
    For Each filename In filetoopen
        import_shmfile filename
        rawShmoo = import_shmfile(filename)
        
        obj_raw_shmoo_sheet.AddRawShmoo Dir(filename), rawShmoo
    Next
    
End Sub

Sub CreateEmptyRawShmoo()

    Dim cProps As CExcelProperties
    Set cProps = New CExcelProperties
    Application.ScreenUpdating = False

    '-----------------------------------
    Dim sht_raw_shmoo As Worksheet
    Set sht_raw_shmoo = ActiveSheet

    Dim obj_raw_shmoo_sheet As CRawShmooSheet
    Set obj_raw_shmoo_sheet = New CRawShmooSheet
    obj_raw_shmoo_sheet.Sheet = sht_raw_shmoo

    If obj_raw_shmoo_sheet.IsRawShmooSheet And Not obj_raw_shmoo_sheet.IsEmpty _
    Then
        ' Do nothing
    Else
        MsgBox "Please begin on an non-empty raw shmoo sheet."
        Exit Sub
    End If

    Dim clct_raw_shmoos As Collection
    Dim nm_shmoo
    Set clct_raw_shmoos = obj_raw_shmoo_sheet.RawShmoos
    nm_shmoo = False

    Do
        If nm_shmoo = False Then nm_shmoo = clct_raw_shmoos(clct_raw_shmoos.Count)
        nm_shmoo = InputBox("Input name for empty shmoo. " & vbCrLf _
            & "The name must be different to existent shmoos." _
            , _
            "HP93000 Shmoo Excel Tool", nm_shmoo)
        
        If nm_shmoo = "" Then Exit Sub
    Loop Until obj_raw_shmoo_sheet.ShmooInfoDict(nm_shmoo) Is Nothing
    
    Call obj_raw_shmoo_sheet.AddEmptyRawShmoo(nm_shmoo)

End Sub

Sub RawShmooRegenerateSelectingLinks()

    Dim cProps As CExcelProperties
    Set cProps = New CExcelProperties
    Application.ScreenUpdating = False

    '-----------------------------------
    Dim sht_raw_shmoo As Worksheet
    Set sht_raw_shmoo = ActiveSheet

    Dim obj_raw_shmoo_sheet As CRawShmooSheet
    Set obj_raw_shmoo_sheet = New CRawShmooSheet
    obj_raw_shmoo_sheet.Sheet = sht_raw_shmoo

    If obj_raw_shmoo_sheet.IsRawShmooSheet And Not obj_raw_shmoo_sheet.IsEmpty _
    Then
        ' Do nothing
    Else
        MsgBox "Please begin on an non-empty raw shmoo sheet."
        Exit Sub
    End If

    If sht_raw_shmoo.Parent Is ThisWorkbook Then
        MsgBox "Please do not begin from raw shmoo sheet of this HP93kShmooExcel Workbook."
        Exit Sub
    End If

    obj_raw_shmoo_sheet.RegenerateAllShmooLinks

End Sub

Sub DefineShmooMatrixDirectionSize()

    Dim cProps As CExcelProperties
    Set cProps = New CExcelProperties
    Application.ScreenUpdating = False

    '-----------------------------------
    Dim sht As Worksheet
    Set sht = ActiveSheet

    If sht.Parent Is ThisWorkbook Then
        MsgBox "Please do not begin from this HP93kShmooExcel Workbook."
        Exit Sub
    End If

    Dim obj_raw_shmoo_sheet As CRawShmooSheet
    Set obj_raw_shmoo_sheet = New CRawShmooSheet
    obj_raw_shmoo_sheet.Sheet = sht
    
    Dim scope
    If obj_raw_shmoo_sheet.IsRawShmooSheet Then
        scope = MsgBox("The scope for shmoo matrix definition: workbook?" & vbCrLf & _
            "  Yes => workbook" & vbCrLf & _
            "  No  => sheet" & vbCrLf, vbYesNoCancel + vbDefaultButton1, "HP93000 Shmoo Excel Tool")
    
        Select Case scope
            Case vbYes: scope = "workbook"
            Case vbNo:  scope = "sheet"
            Case Else:  Exit Sub
        End Select
    Else
        scope = "workbook"
    End If
    
    
    ' get default matrix bound for setting
    Dim matrix_bound
    Dim matrix_bound_bk     ' workbook-scope matrix bound
    Dim matrix_bound_sht    ' sheet-scope matrix bound
    Dim t_matrix_bound      ' to get default matrix bound for setting
    
    On Error Resume Next
    If scope = "sheet" Then
        t_matrix_bound = Evaluate(sht.Names("matrix_bound").Value)
        If Err.Number <> 0 Then
            Err.Clear
            matrix_bound_sht = ""
            t_matrix_bound = Evaluate(sht.Parent.Names("matrix_bound").Value)
            If Err.Number <> 0 Then
                Err.Clear
                matrix_bound_bk = ""
                t_matrix_bound = "w5"
            Else
                matrix_bound_bk = t_matrix_bound
            End If
        Else
            matrix_bound_sht = t_matrix_bound
            matrix_bound_bk = Evaluate(sht.Parent.Names("matrix_bound").Value)
            If Err.Number <> 0 Then
                Err.Clear
                matrix_bound_bk = ""
            End If
        End If
    Else
        t_matrix_bound = Evaluate(sht.Parent.Names("matrix_bound").Value)
        If Err.Number <> 0 Then
            Err.Clear
            matrix_bound_bk = ""
            t_matrix_bound = "w5"
        Else
            matrix_bound_bk = t_matrix_bound
        End If
    End If
    matrix_bound = t_matrix_bound
    
    On Error GoTo 0
    
    ' desired matrix definition from users
    Do While True
        Dim t
        t = "Shmoo matrix settings:" & vbCrLf _
            & "  direction: 'h' or 'w'" & vbCrLf _
            & "  bound size: a positive integer" & vbCrLf _
            & "  e.g. h6, w5." & vbCrLf _
            & "Exceptions: " & vbCrLf _
            & "  'h0': the first form of raw shmoo." & vbCrLf _
            & IIf(scope = "sheet", "  'wb': as workbook scope." & vbCrLf, "") _
            & "scope: " & scope & vbCrLf _
            & "workbook" & IIf(scope = "sheet", " / sheet", "") _
            & ": [" & matrix_bound_bk & IIf(scope = "sheet", "]/[" & matrix_bound_sht, "") & "]" & vbCrLf
            
        matrix_bound = Application.InputBox(t _
            , _
            "HP93000 Shmoo Excel Tool", matrix_bound, , , , , 2)
            
        ' Cancel
        If matrix_bound = False Then
            Exit Sub
        End If
        
        Dim mb_direction, mb_size
        
        matrix_bound = LCase(matrix_bound)
        If matrix_bound = "wb" Then
            matrix_bound = matrix_bound_bk
            Exit Do
        ElseIf matrix_bound = "" Then
            Exit Do
        End If
        
        mb_direction = Left(matrix_bound, 1)
        mb_size = Int(Val(Mid(matrix_bound, 2)))
        
        If ((mb_direction = "h" Or mb_direction = "w") And mb_size > 0) Or matrix_bound = "h0" Then
            If mb_direction & mb_size = matrix_bound Then Exit Do
        
        End If
            
        matrix_bound = t_matrix_bound
    Loop

    ' workbook scope: do not rearrange
    ' raw shmoo: if change => force re-arrange
    '               the same => do not re-arrange
    
    If scope = "workbook" Then
        Dim bk As Workbook
        Set bk = ActiveSheet.Parent
        Call bk.Names.Add("matrix_bound", matrix_bound)
    Else
        ' old matrix-bound
        On Error Resume Next
        t_matrix_bound = Evaluate(sht.Names("matrix_bound").Value)
        If Err.Number <> 0 Then
            Err.Clear
            t_matrix_bound = ""
        End If
        On Error GoTo 0
        
        Call obj_raw_shmoo_sheet.ReArrangeMatrix(matrix_bound)
    End If

End Sub

Sub NoteShmooPassRegion()

    Dim cProps As CExcelProperties
    Set cProps = New CExcelProperties
    Application.ScreenUpdating = False

    '-----------------------------------
    Dim sht As Worksheet
    Set sht = ActiveSheet

    If sht.Parent Is ThisWorkbook Then
        MsgBox "Please do not begin from this HP93kShmooExcel Workbook."
        Exit Sub
    End If

    Dim obj_raw_shmoo_sheet As CRawShmooSheet
    Set obj_raw_shmoo_sheet = New CRawShmooSheet
    obj_raw_shmoo_sheet.Sheet = sht
    
    Call obj_raw_shmoo_sheet.AddShmooPassValues
    
End Sub

Sub CreateOverlayShmoo()

    Dim cProps As CExcelProperties
    Set cProps = New CExcelProperties
    Application.ScreenUpdating = False
    
    '-----------------------------------
    Dim sht_raw_shmoo As Worksheet
    Set sht_raw_shmoo = ActiveSheet
    
    Dim obj_raw_shmoo_sheet As CRawShmooSheet
    Set obj_raw_shmoo_sheet = New CRawShmooSheet
    obj_raw_shmoo_sheet.Sheet = sht_raw_shmoo
    
    If Not obj_raw_shmoo_sheet.IsRawShmooSheet _
        Or obj_raw_shmoo_sheet.IsEmpty _
    Then
        MsgBox "Please begin on a non-empty raw shmoo sheet. ""OVLxxx"" is also not allowed."
        Exit Sub
    End If
    
    If sht_raw_shmoo.Parent Is ThisWorkbook Then
        MsgBox "Please do not begin from raw shmoo sheet of this HP93kShmooExcel Workbook."
        Exit Sub
    End If
    
    Dim bk_raw_shmoo As Workbook
    Set bk_raw_shmoo = sht_raw_shmoo.Parent
    
    ' ensure not result overlay shmoo sheet [OVL]
    Dim sht_overlay_shmoo As Worksheet
    Set sht_overlay_shmoo = Nothing
    On Error Resume Next
    Set sht_overlay_shmoo = bk_raw_shmoo.Worksheets("OVL")
    On Error GoTo 0
    
    If Not sht_overlay_shmoo Is Nothing Then
        MsgBox "There is already an ""OVL"". Please rename it first."
        Exit Sub
    End If
    
    Set sht_overlay_shmoo = bk_raw_shmoo.Worksheets.Add(, sht_raw_shmoo)
    sht_overlay_shmoo.Name = "OVL"
        
    ' generate overlay shmoo
    Dim clct_source_shmoo As Collection
    Set clct_source_shmoo = New Collection
    
    Dim filename
    For Each filename In obj_raw_shmoo_sheet.RawShmoos
        clct_source_shmoo.Add Array(sht_raw_shmoo.Name, filename)
    Next
    
    generate_overlay_shmoo2 sht_overlay_shmoo, clct_source_shmoo
    
End Sub

Sub AddRawShmoosToOverlayShmoo()
    Dim cProps As CExcelProperties
    Set cProps = New CExcelProperties
    
    '-----------------------------------
    Dim sht_overlay_shmoo As Worksheet
    Set sht_overlay_shmoo = ActiveSheet
    If sht_overlay_shmoo.Cells(1, 1).Value <> "Overlay shmoos" Then
        MsgBox "Please start in an overlay shmoo sheet"
        Exit Sub
    End If
    
    
    Dim obj_raw_shmoo_sheet As CRawShmooSheet
    Set obj_raw_shmoo_sheet = New CRawShmooSheet
    
    Dim sht_raw_shmoo As Worksheet
    Dim name_sheet
    Dim t
    name_sheet = ""
    t = ""
    
    Application.ScreenUpdating = True
    Do
        Dim rng As Range
        On Error Resume Next
        Set rng = Application.InputBox(t & "Select any cell in the raw shmoo sheet you want:", _
            "HP93000 Shmoo Excel Tool", Type:=8)
        On Error GoTo 0
        If rng Is Nothing Then Exit Sub
        
        Set sht_raw_shmoo = rng.Parent
        obj_raw_shmoo_sheet.Sheet = sht_raw_shmoo
        
        If Not obj_raw_shmoo_sheet.IsRawShmooSheet _
            Or obj_raw_shmoo_sheet.IsEmpty _
        Then
            t = "Not a non-empty raw shmoo sheet => " & sht_raw_shmoo.Name & vbCrLf
        Else
            Exit Do
        End If
    Loop
    Application.ScreenUpdating = False
        
    ' generate overlay shmoo
    Dim clct_source_shmoo As Collection
    Set clct_source_shmoo = New Collection
    
    Dim filename
    For Each filename In obj_raw_shmoo_sheet.RawShmoos
        clct_source_shmoo.Add Array(sht_raw_shmoo.Name, filename)
    Next
    
    generate_overlay_shmoo2 sht_overlay_shmoo, clct_source_shmoo

End Sub

Sub OverlayShmooReRun()
    Dim cProps As CExcelProperties
    Set cProps = New CExcelProperties
    
    '-----------------------------------
    Dim sht_overlay_shmoo As Worksheet
    Set sht_overlay_shmoo = ActiveSheet
    If sht_overlay_shmoo.Cells(1, 1).Value <> "Overlay shmoos" Then
        MsgBox "Please start in an overlay shmoo sheet"
        Exit Sub
    End If
    
    Application.ScreenUpdating = False
    
    ' generate overlay shmoo
    Dim clct_source_shmoo As Collection
    Set clct_source_shmoo = New Collection
    If sht_overlay_shmoo.AutoFilterMode = True Then sht_overlay_shmoo.Cells.AutoFilter
    clct_source_shmoo.Add Array(sht_overlay_shmoo.Cells(12, 3).Value, sht_overlay_shmoo.Cells(12, 4).Value)
    generate_overlay_shmoo2 sht_overlay_shmoo, clct_source_shmoo

End Sub

Sub DefineColorMap()
    Dim cProps As CExcelProperties
    Set cProps = New CExcelProperties
    
    '-----------------------------------
    Dim sht As Worksheet
    
    Dim obj_raw_shmoo_sheet As CRawShmooSheet
    Set obj_raw_shmoo_sheet = New CRawShmooSheet
    
    Dim t: t = ""
    Application.ScreenUpdating = True
    Do
        Dim rng As Range
        Dim r As Range
        Set rng = Nothing
        On Error Resume Next
        Set rng = Application.InputBox(t & "Select a range (n rows x 1 column) that defines color map", _
            "HP93000 Shmoo Excel Tool", Type:=8)
        On Error GoTo 0
        If rng Is Nothing Then Exit Sub
        
        '--- check validation of selected range for color map
        ' column count
        If rng.Columns.Count <> 1 Then
            t = "The range of color map should have exact 1 column. Column count => " & rng.Columns.Count & vbCrLf
            GoTo SelectColorMapRange
        End If
        
        ' minimum and maximum. also implies at least two distinct numbers.
        If WorksheetFunction.Max(rng) <= 0 _
            Or WorksheetFunction.Min(rng) > 0 _
        Then
            t = "Need a maximum>0 and a minimum<=0. " _
                & "Max => " & Round(WorksheetFunction.Max(rng), 4) & ". " _
                & "Min => " & Round(WorksheetFunction.Min(rng), 4) & ". " _
                & vbCrLf
            GoTo SelectColorMapRange
        End If
        
        ' at least two colors for numeric cells
        Dim dict As Dictionary
        Set dict = New Dictionary
        For Each r In rng
            If IsNumeric(r.Value) Then
                dict.Item(r.DisplayFormat.Interior.Color) = ""
            End If
        Next
        If dict.Count < 2 Then
            t = "Please define at least 2 colors" & vbCrLf
            GoTo SelectColorMapRange
        End If
        
        '--- detect workbook-scope or sheet-scope
        Set sht = rng.Parent
        obj_raw_shmoo_sheet.Sheet = sht
        
        If obj_raw_shmoo_sheet.IsRawShmooSheet _
            Or sht.Cells(1, 1).Value = "Overlay shmoos" _
            Or Left(sht.Name, 3) = "OVL" _
        Then
            ' sheet-scope
            t = MsgBox("Sheet-scope color map: " & rng.Address & vbCrLf _
                    & "Yes    => define new color map  and overwrite the existent" & vbCrLf _
                    & "No     => reselect a new range" & vbCrLf _
                    & "Cancel => exit without selecting", _
                    vbYesNoCancel, "HP93000 Shmoo Excel Tool")
            
            If t = vbCancel Then Exit Sub
            If t = vbYes Then
                Call sht.Names.Add("ColorMap", "=" & rng.Address)
                Exit Do
            End If
            t = ""
        Else
            ' workbook-scope
            t = MsgBox("Workbook-scope color map: '" & sht.Name & "'!" & rng.Address & vbCrLf _
                    & "Yes    => define new color map  and overwrite the existent" & vbCrLf _
                    & "No     => reselect a new range" & vbCrLf _
                    & "Cancel => exit without selecting", _
                    vbYesNoCancel, "HP93000 Shmoo Excel Tool")
            
            If t = vbCancel Then Exit Sub
            If t = vbYes Then
                Dim bk As Workbook
                Set bk = sht.Parent
                Call bk.Names.Add("ColorMap", "='" & sht.Name & "'!" & rng.Address)
                Exit Do
            End If
            t = ""
        End If
SelectColorMapRange:
    Loop
    Application.ScreenUpdating = False
        
End Sub

Sub RemoveWorkbookColorMap()

    Dim t
    t = MsgBox("Remove workbook color map?", _
            vbOKCancel, "HP93000 Shmoo Excel Tool")
    
    If t = vbCancel Then Exit Sub
    
    Dim nm As Name
    Dim clct_to_delete As Collection
    Set clct_to_delete = New Collection
    For Each nm In ActiveWorkbook.Names
        If nm.Name = "ColorMap" Then
            clct_to_delete.Add nm
        End If
    Next
    
    For Each nm In clct_to_delete
        nm.Delete
    Next
    
End Sub

Sub RemoveSheetColorMap()

    '-----------------------------------
    Dim sht As Worksheet
    
    Dim obj_raw_shmoo_sheet As CRawShmooSheet
    Set obj_raw_shmoo_sheet = New CRawShmooSheet
    
    Set sht = ActiveSheet
    obj_raw_shmoo_sheet.Sheet = sht
    
    If obj_raw_shmoo_sheet.IsRawShmooSheet _
        Or sht.Cells(1, 1).Value = "Overlay shmoos" _
        Or Left(sht.Name, 3) = "OVL" _
    Then
        ' NO-OP
    Else
        MsgBox "Please start from a raw/overlay shmoo sheet"
        Exit Sub
    End If
    
    
    Dim t
    t = MsgBox("Remove sheet-scope color map?", _
            vbOKCancel, "HP93000 Shmoo Excel Tool")
    
    If t = vbCancel Then Exit Sub
    
    Dim nm As Name
    On Error Resume Next
    Set nm = sht.Names("ColorMap")
    If nm.RefersToRange.Parent.Name = sht.Name Then nm.Delete
    On Error GoTo 0
    
End Sub

Sub DumpExampleColorMap()

    Dim cProps As CExcelProperties
    Set cProps = New CExcelProperties
    Application.ScreenUpdating = False
    
    '-----------------------------------
    Dim sht As Worksheet
    Set sht = ActiveSheet
    
    If sht.Parent Is ThisWorkbook Then
        MsgBox "Please do not begin from any sheet of this HP93kShmooExcel Workbook."
        Exit Sub
    End If
    
    If UCase(Left(sht.Name, 4)) = "OVL-" Or _
        UCase(sht.Name) = "OVL" Or _
        Not (IsEmptySheet(sht)) _
    Then
        MsgBox "Please begin on an empty worksheet. Sheet OVL-xxxx or OVL is also not allowed."
        Exit Sub
    End If
    
    '--- dump exmaple color maps
    
    Dim rng As Range
    
    Set rng = sht.Cells(2, 1)
    
    Set rng = rng.Offset(, 2): Call DumpColorMap_3ColorScaleGreenYellowRed(rng)
    Set rng = rng.Offset(, 2): Call DumpColorMap_3ColorScaleBlueWhiteRed(rng)
    Set rng = rng.Offset(, 2): Call DumpColorMap_2ColorScaleGreenRed(rng)
    Set rng = rng.Offset(, 2): Call DumpColorMap_Simple5(rng)
    Set rng = rng.Offset(, 2): Call DumpColorMap_Simple11(rng)
    
End Sub

Sub NotImplementedYet()

    MsgBox "This function is not implemented yet."
    
End Sub

' __END__
