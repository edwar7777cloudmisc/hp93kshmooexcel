Option Explicit

Private Sub Workbook_BeforeClose(Cancel As Boolean)
   With Application.CommandBars("Worksheet Menu Bar")
      On Error Resume Next
      .Controls("&HP93kShmooExcel").Delete
      On Error GoTo 0
   End With
End Sub

Private Sub Workbook_Open()
    Dim objPopUp As CommandBarPopup
    Dim objBtn As CommandBarButton
    Dim objPopUpSub As CommandBarPopup
    With Application.CommandBars("Worksheet Menu Bar")
        On Error Resume Next
        .Controls("HP93kShmooExcel").Delete
        On Error GoTo 0
        Set objPopUp = .Controls.Add( _
            Type:=msoControlPopup, _
            before:=.Controls.Count, _
            temporary:=True)
    End With
    objPopUp.Caption = "HP93kShmooExcel"
   
    ' ---- Raw Shmoo
    Set objPopUpSub = objPopUp.Controls.Add(msoControlPopup)
    With objPopUpSub
        .Caption = "Raw Shmoo"
        With .Controls.Add(msoControlButton)
            .Caption = "Create new one from 93k shmoo .txt files"
            .OnAction = "CreateRawShmooFromFiles"
        End With
        With .Controls.Add(msoControlButton)
            .Caption = "Add new 93k shmoo .txt files"
            .OnAction = "AddRawShmooFromFiles"
        End With
        With .Controls.Add(msoControlButton)
            .Caption = "Create empty raw shmoo"
            .OnAction = "CreateEmptyRawShmoo"
        End With
        With .Controls.Add(msoControlButton)
            .Caption = "New color map (raw overlay only)"
            .Enabled = False
        End With
        With .Controls.Add(msoControlButton)
            .Caption = "Recreate 'Select all' link"
            .OnAction = "RawShmooRegenerateSelectingLinks"
            .Enabled = True
        End With
        With .Controls.Add(msoControlButton)
            .Caption = "Shmoo matrix - direction / size / re-sort"
            .OnAction = "DefineShmooMatrixDirectionSize"
            .Enabled = True
        End With
        With .Controls.Add(msoControlButton)
            .Caption = "Note shmoo pass region"
            .OnAction = "NoteShmooPassRegion"
            .Enabled = True
        End With
    End With

    ' --- Overlay Shmoo
    Set objPopUpSub = objPopUp.Controls.Add(msoControlPopup)
    With objPopUpSub
        .Caption = "Overlay Shmoo"
        With .Controls.Add(msoControlButton)
            .Caption = "Create new sheet ""OVL"" (from raw shmoo sheet)"
            .OnAction = "CreateOverlayShmoo"
        End With
        With .Controls.Add(msoControlButton)
            .Caption = "Add raw shmoo sheet (from overlay shmoo sheet)"
            .OnAction = "AddRawShmoosToOverlayShmoo"
        End With
        With .Controls.Add(msoControlButton)
            .Caption = "Re-run overlay"
            .OnAction = "OverlayShmooReRun"
        End With
    End With

    ' --- Color Map
    Set objPopUpSub = objPopUp.Controls.Add(msoControlPopup)
    With objPopUpSub
        .Caption = "Color Map"
        With .Controls.Add(msoControlButton)
            .Caption = "Define workbook or sheet color map"
            .OnAction = "DefineColorMap"
        End With
        With .Controls.Add(msoControlButton)
            .Caption = "Remove workbook color Map"
            .OnAction = "RemoveWorkbookColorMap"
        End With
        With .Controls.Add(msoControlButton)
            .Caption = "Remove color map in raw/overlay shmoo sheet"
            .OnAction = "RemoveSheetColorMap"
        End With
        With .Controls.Add(msoControlButton)
            .Caption = "Dump example color map"
            .OnAction = "DumpExampleColorMap"
        End With
    End With
End Sub
