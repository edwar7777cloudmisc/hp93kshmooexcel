'Option Explicit

dim fso: set fso = CreateObject("Scripting.FileSystemObject")
dim current_dir
dim xlsm_name
current_dir = fso.GetAbsolutePathName(".")
xlsm_name = current_dir + "\HP93kShmooExcel.xlsm"

'WScript.Quit
'------------------

Dim OXL
Set OXL = CreateObject("Excel.Application") 

OXL.Application.Visible = True

Dim bk_xlsm, VBP, VBModule
Set bk_xlsm = OXL.Workbooks.add

With bk_xlsm.Sheets(1)
    .Range("B2").Value = "Source codes and document:"
    .Range("B3").Value = "* BitBucket (old, no more update)"
    .Hyperlinks.Add .Range("B4"), "https://bitbucket.org/edwar7777cloudmisc/hp93kshmooexcel"
    .Range("B5").Value = "* SourceForge"
    .Hyperlinks.Add .Range("B6"), "https://sourceforge.net/projects/hp93kshmooexcel"
End With

On Error Resume Next
Set VBP = Nothing
Set VBP = bk_xlsm.VBProject

If Err.Number <> 0 Then
    Err.Clear
    MsgBox _
    "Programmatic access to Visual Basic Project is not allowed." & vbCrLf _
    & "The VBA macro codes will not be imported." & vbCrLf _
    & vbCrLf _
    & "Please import manually. " _
    & "Or enable 'Trust access to the VBA project object model' in Trust Center -> Macro Settings,"  _
    & "and try again."
End if

If Not VBP Is Nothing Then
    With VBP.VBComponents("ThisWorkbook").CodeModule
        .DeleteLines 1, .CountOfLines
        .AddFromFile current_dir + "\" + "ThisWorkbook.sheet.cls"
    End With

    For Each oFile In fso.GetFolder(current_dir).Files
        If _
            UCase(fso.GetExtensionName(oFile.Name)) = "CLS" _
            Or UCase(fso.GetExtensionName(oFile.Name)) = "BAS" _
            Then
            If UCase(fso.GetBaseName(oFile.Name)) <> UCase("ThisWorkbook.sheet") Then
                VBP.VBComponents.Import fso.GetFile(oFile.Name)
            End if
        End if
    Next

    MsgBox _
    "Please rememeber to check 'Microsoft Scripting Runtime' in " _
    & "VBAProject -> Tools -> Reference to run the program. " _
    & "You may also want to restore 'Trust access to the VBA project object model' to its original setting."
End If

OXL.Application.DisplayAlerts = False
'OXL.Activeworkbook.SaveAs FileName:=xlsm_name, FileFormat:=xlOpenXMLWorkbookMacroEnabled
OXL.Activeworkbook.SaveAs xlsm_name, 52
OXL.Activeworkbook.Close

OXL.Quit

'__END__
' vim: ts=4:sw=4:
