Attribute VB_Name = "module_shmoo_plot"
Option Explicit

Private Enum V93kTitleSplitTypes
    unit
    main_title
    pins
    ArrayAll
End Enum

Sub SetOuterThinLine(rng As Range)

    Dim t ' As XlBordersIndex
    
    Dim a As Variant
    a = Array(xlEdgeLeft, xlEdgeTop, xlEdgeBottom, xlEdgeRight)
    
    With rng
        .Borders(xlDiagonalDown).LineStyle = xlNone
        .Borders(xlDiagonalUp).LineStyle = xlNone
        For Each t In a
            .Borders(t).LineStyle = xlContinuous
            .Borders(t).ColorIndex = 0
            .Borders(t).TintAndShade = 0
            .Borders(t).Weight = xlThin
        Next
        .Borders(xlInsideVertical).LineStyle = xlNone
        .Borders(xlInsideHorizontal).LineStyle = xlNone
    End With

End Sub

Private Function format_V93k_shmoo_title(orig_title)

    Dim result
    Dim i0
    result = V93k_parameter_split(orig_title, ArrayAll)
    i0 = LBound(result)
    format_V93k_shmoo_title = result(i0 + 0) & " " & result(i0 + 1) & " [" & result(i0 + 2) & "]"

End Function

Private Function V93k_parameter_split(orig_title, Optional ByVal part As V93kTitleSplitTypes = ArrayAll) As Variant

    Dim result
    Dim i0
    result = Split(orig_title, "*")
    i0 = LBound(result)
    
    Select Case part
        Case unit
            V93k_parameter_split = result(i0 + 3)
        Case main_title
            V93k_parameter_split = result(i0 + 0)
        Case pins
            V93k_parameter_split = result(i0 + 1)
        Case ArrayAll
            V93k_parameter_split = Array(result(i0 + 0), result(i0 + 1), result(i0 + 3))
    End Select

End Function

Sub setChartTitle(rng_shmoo_plot As Range, Title)

    ' chart title
    Dim factor_size
    With rng_shmoo_plot
        factor_size = 1
        factor_size = WorksheetFunction.Max(factor_size, .Rows.Count / 60)
        factor_size = WorksheetFunction.Max(factor_size, .Columns.Count / 80)
    End With
    With rng_shmoo_plot.Offset(-2).Resize(1)
        .HorizontalAlignment = xlCenter
        .VerticalAlignment = xlCenter
        .Font.Size = Int(14 * factor_size)
        .Merge
        .Value = Title
    End With

End Sub

Sub setYAxis(rng_shmoo_plot As Range, y_values As Variant, y_axis_title, Optional gsy = 1)

    ' y-axis title
    Dim factor_size
    With rng_shmoo_plot
        factor_size = 1
        factor_size = WorksheetFunction.Max(factor_size, .Rows.Count / 60)
        factor_size = WorksheetFunction.Max(factor_size, .Columns.Count / 80)
    End With
    With rng_shmoo_plot.Offset(, -3).Resize(, 1)
        .HorizontalAlignment = xlCenter
        .VerticalAlignment = xlCenter
        .Font.Size = Int(14 * factor_size)
        .Orientation = xlVertical
        .ReadingOrder = xlContext
        .Merge
        .Value = format_V93k_shmoo_title(y_axis_title)
    End With

    Dim unit
    unit = V93k_parameter_split(y_axis_title, unit)

    ' y-axis tick marks
    Dim o
    Dim xymax, xymin, xysteps
    Dim nY
    Dim sht As Worksheet
    Dim r0, c0
    Dim is_hex, symbol_hex
    Dim t
    is_hex = False
    symbol_hex = ""
    
    xymin = y_values(LBound(y_values))
    xymax = y_values(LBound(y_values) + 1)
    
    ' deal with hexadecimal values
    If Left(xymin, 1) = "#" Then
        is_hex = True
        symbol_hex = "#"
        xymin = Val("&h" & Mid(xymin, 2))
    ElseIf LCase(Left(xymin, 2)) = "&h" Or LCase(Left(xymin, 2)) = "0x" Then
        is_hex = True
        symbol_hex = Left(xymin, 2)
        xymin = Val("&h" & Mid(xymin, 3))
    End If
    
    If Left(xymax, 1) = "#" Then
        is_hex = True
        If symbol_hex = "" Then symbol_hex = "#"
        xymax = Val("&h" & Mid(xymax, 2))
    ElseIf LCase(Left(xymax, 2)) = "&h" Or LCase(Left(xymax, 2)) = "0x" Then
        is_hex = True
        If symbol_hex = "" Then symbol_hex = Left(xymax, 2)
        xymax = Val("&h" & Mid(xymax, 3))
    End If
    
    xysteps = rng_shmoo_plot.Rows.Count / gsy - 1
    xysteps = IIf(xysteps > 0, xysteps, 1)
    
    With rng_shmoo_plot
        nY = .Rows.Count / gsy
        Set sht = .Parent
        r0 = .Row
        c0 = .Column
    End With
    
    For o = 1 To nY
        With sht.Cells(r0 + (nY - o) * gsy, c0 - 1)
        
            If o Mod 5 = 1 Then
                ' major ticks
                .Font.Name = "Segoe UI Black"
                .Font.Size = 14
                
                .HorizontalAlignment = xlLeft
                If gsy = 1 Then
                    .VerticalAlignment = xlBottom
                End If
                .WrapText = False
                .Orientation = 90
                .AddIndent = False
                .IndentLevel = 0
                .ShrinkToFit = False
                .ReadingOrder = xlContext
                
                .Value = "|"
            Else
                ' minor ticks
                .Font.Name = "Calibri"
                .Font.Size = 11
                .Value = "   �X"
            End If
            
            .Resize(gsy).MergeCells = True
            
        End With
    Next
        
    ' y-axis tick values, incl. Div
    For o = 1 To nY
        With sht.Cells(r0 + (nY - o) * gsy, c0 - 2)
        
            If o Mod 5 = 1 Then
                ' major ticks
                
                t = xymin + (xymax - xymin) / xysteps * (o - 1)
                .Value = IIf(is_hex, symbol_hex & Hex$(t), t)
            End If
            
            .Resize(gsy).MergeCells = True
            
            ' each hidden tick value
            With .Offset(, rng_shmoo_plot.Columns.Count + 2)
                t = xymin + (xymax - xymin) / xysteps * (o - 1)
                .Value = IIf(is_hex, symbol_hex & Hex$(t), t)
                .Resize(gsy).MergeCells = True
            End With
            
        End With
    Next
    
    With rng_shmoo_plot.Resize(, 1).Offset(, -2)
        .Font.Size = 12
        .HorizontalAlignment = xlRight
        
        ' hide all tick value at the right side
        Call HideFont(.Offset(, rng_shmoo_plot.Columns.Count + 2))
        
        With .Resize(1, 1).Offset(-2, -2).Resize(1, 3)
            .MergeCells = True
            .Font.Size = 12
            t = (xymax - xymin) / xysteps
            t = IIf(is_hex And t >= 10, symbol_hex & Hex$(t), t)
            .Value = t & " " & unit & "/Div."
            .HorizontalAlignment = xlRight
        End With
    
    End With
    
End Sub

Sub setXAxis(rng_shmoo_plot As Range, x_values As Variant, x_axis_title, Optional gsx = 1, Optional gsy = 1)

    Dim nX, nY
    Dim sht As Worksheet
    Dim r0, c0
    Dim factor_size
    
    With rng_shmoo_plot
        nX = .Columns.Count / gsx
        nY = .Rows.Count / gsy
        Set sht = .Parent
        r0 = .Row
        c0 = .Column
        factor_size = 1
        factor_size = WorksheetFunction.Max(factor_size, .Rows.Count / 60)
        factor_size = WorksheetFunction.Max(factor_size, .Columns.Count / 80)
    End With
    
    ' x-axis title
    With rng_shmoo_plot.Resize(1).Offset(nY * gsy + 3)
        .HorizontalAlignment = xlCenter
        .VerticalAlignment = xlCenter
        .Font.Size = Int(14 * factor_size)
        .Merge
        .Value = format_V93k_shmoo_title(x_axis_title)
    End With
    
    Dim unit
    unit = V93k_parameter_split(x_axis_title, unit)

    ' x-axis tick marks
    Dim o
    Dim xymax, xymin, xysteps
    Dim is_hex, symbol_hex
    is_hex = False
    symbol_hex = ""
    
    xymin = x_values(LBound(x_values))
    xymax = x_values(LBound(x_values) + 1)
    
    ' deal with hexadecimal values
    If Left(xymin, 1) = "#" Then
        is_hex = True
        symbol_hex = "#"
        xymin = Val("&h" & Mid(xymin, 2))
    ElseIf LCase(Left(xymin, 2)) = "&h" Or LCase(Left(xymin, 2)) = "0x" Then
        is_hex = True
        symbol_hex = Left(xymin, 2)
        xymin = Val("&h" & Mid(xymin, 3))
    End If
    
    If Left(xymax, 1) = "#" Then
        is_hex = True
        If symbol_hex = "" Then symbol_hex = "#"
        xymax = Val("&h" & Mid(xymax, 2))
    ElseIf LCase(Left(xymax, 2)) = "&h" Or LCase(Left(xymax, 2)) = "0x" Then
        is_hex = True
        If symbol_hex = "" Then symbol_hex = Left(xymax, 2)
        xymax = Val("&h" & Mid(xymax, 3))
    End If
    
    xysteps = nX - 1
    xysteps = IIf(xysteps > 0, xysteps, 1)
    
    For o = 1 To nX
        With sht.Cells(r0 + nY * gsy, c0 + (o - 1) * gsx)
        
            If o Mod 5 = 1 Then
                ' major ticks
                .Font.Name = "Bauhaus 93"
                .Font.Size = 11
                
                .HorizontalAlignment = xlCenter
                .VerticalAlignment = xlBottom
                
                .Value = "|"
            Else
                ' minor ticks
                .Font.Name = "Calibri"
                .Font.Size = 11
                
                .HorizontalAlignment = xlCenter
                .VerticalAlignment = xlBottom
                .WrapText = False
                .Orientation = xlVertical
                .AddIndent = False
                .IndentLevel = 0
                .ShrinkToFit = False
                .ReadingOrder = xlContext
                
                .Value = "I"
            End If
            
            .Resize(, gsx).MergeCells = True
            
        End With
    Next
        
    Dim t, t2
    
    ' x-axis tick values, incl. Div
    For o = 1 To nX
        With sht.Cells(r0 + nY * gsy + 1, c0 + (o - 1) * gsx)
        
            Set t = Nothing
            If gsx = 1 Then
                If o = 1 Then
                    Set t = .Offset(, -1).Resize(, 4)
                ElseIf o Mod 10 = 1 Then
                    Set t = .Offset(, -2).Resize(, 5)
                End If
            ElseIf gsx = 2 Then
                If o = 1 Then
                    Set t = .Offset(, -1).Resize(, 4)
                ElseIf o Mod 10 = 1 Then
                    Set t = .Offset(, -gsx).Resize(, 3 * gsx)
                End If
            ElseIf gsx = 3 Then
                If o = 1 Then
                    Set t = .Offset(, -1).Resize(, 5)
                ElseIf o Mod 10 = 1 Then
                    Set t = .Offset(, -1).Resize(, 5)
                End If
            ElseIf gsx = 4 Then
                If o = 1 Then
                    Set t = .Offset(, -1).Resize(, 6)
                ElseIf o Mod 10 = 1 Then
                    Set t = .Offset(, -1).Resize(, 6)
                End If
            ElseIf gsx >= 5 Then
                If o = 1 Then
                    Set t = .Resize(, gsx)
                ElseIf o Mod 10 = 1 Then
                    Set t = .Resize(, gsx)
                End If
            End If
            
            If Not t Is Nothing Then
                t.ClearContents
                t.MergeCells = True
                t2 = xymin + (xymax - xymin) / xysteps * (o - 1)
                t.Value = IIf(is_hex, symbol_hex & Hex$(t2), t2)
                t.HorizontalAlignment = xlCenter
                t.Font.Size = 12
            End If
            
        End With
        
        ' each hidden tick value
        With sht.Cells(r0 - 1, c0 + (o - 1) * gsx)
            t = xymin + (xymax - xymin) / xysteps * (o - 1)
            .Value = IIf(is_hex, symbol_hex & Hex$(t), t)
            .Resize(, gsx).MergeCells = True
        End With
    Next
        
    ' hide all tick value at the top side
    Call HideFont(rng_shmoo_plot.Resize(1).Offset(-1))    '.NumberFormatLocal = ";;;"
    
    ' show Div.
    With sht.Cells(r0 + nY * gsy + 2, c0 + nX * gsx - 1)
        .Font.Size = 12
        t = (xymax - xymin) / xysteps
        t = IIf(is_hex And t >= 10, symbol_hex & Hex$(t), t)
        .Value = t & " " & unit & "/Div."
        .HorizontalAlignment = xlRight
    End With

End Sub

Sub setBorderGray(rng As Range)

    Dim t ' As XlBordersIndex
    
    Dim a As Variant
    a = Array(xlEdgeLeft, xlEdgeTop, xlEdgeBottom, xlEdgeRight, xlInsideVertical, xlInsideHorizontal)
    
    With rng
    For Each t In a
        .Borders(t).ThemeColor = 1
        .Borders(t).TintAndShade = -0.25
    Next
    End With
    
'-0.149998474074526
'-0.249977111117893
'-0.349986266670736

End Sub

Sub setShmoo1RunPassFailLegend(rng_shmoo_plot As Range, _
    Optional fIsDifferential As Boolean = False, _
    Optional clct_color_map As Collection = Nothing)

    ' legend
    With rng_shmoo_plot.Resize(1).Offset(rng_shmoo_plot.Rows.Count + 4)
        .ClearContents
        With .Resize(, 1)
            If fIsDifferential = False Then
                .Offset(, 0).Value = 1
                .Offset(, 1).Value = "failed"
                .Offset(, 7).Value = 0
                .Offset(, 8).Value = "passed"
            Else
                .Offset(, 0).Value = 1
                .Offset(, 1).Value = "Both fail"
                .Offset(, 7).Value = 2 / 3
                .Offset(, 8).Value = "B fail"
                .Offset(, 14).Value = 1 / 3
                .Offset(, 15).Value = "A fail"
                .Offset(, 21).Value = 0
                .Offset(, 22).Value = "Both pass"
            End If
        End With
        .Font.Size = 12
        Call FormatConditionShmooColorMap(.Offset, clct_color_map)
    End With

End Sub

Sub setShmooLegend(rng_shmoo_plot As Range, _
    Optional fIsDifferential As Boolean = False, _
    Optional clct_color_map As Collection = Nothing, _
    Optional fIsOneRunPassFailOnly As Boolean = True)

    If fIsDifferential Or fIsOneRunPassFailOnly Then
        Call setShmoo1RunPassFailLegend(rng_shmoo_plot, fIsDifferential, clct_color_map)
        Exit Sub
    End If
    
    Dim fIsInvalidColorMap As Boolean
    Dim fIsGradient As Boolean
    fIsInvalidColorMap = False
    
    If clct_color_map Is Nothing Then
        fIsInvalidColorMap = True
    ElseIf clct_color_map.Count <= 1 Then
        fIsInvalidColorMap = True
    ElseIf clct_color_map.Count = 2 And TypeName(clct_color_map.Item(clct_color_map.Count)) <> "Variant()" Then
        fIsInvalidColorMap = True
    End If
    
    If fIsInvalidColorMap Then
        fIsGradient = True
    ElseIf TypeName(clct_color_map.Item(clct_color_map.Count)) <> "Variant()" Then
        fIsGradient = True
    Else
        fIsGradient = False
    End If
    
    Dim t_max
    t_max = WorksheetFunction.Max(rng_shmoo_plot)
    If t_max < 1 Then t_max = 1
    
    ' legend
    Dim i, t
    With rng_shmoo_plot.Resize(1).Offset(rng_shmoo_plot.Rows.Count + 4)
        .ClearContents
        .FormatConditions.Delete
        With .Resize(, 1)
            If fIsGradient Then
                .Offset(, 0).Value = 0 * t_max
                Call FormatConditionShmooColorMap(.Offset(, 0), clct_color_map)
                .Offset(, 1).Value = "[" & Round(0 * t_max, 4) & "]"
                
                .Offset(, 7).Value = 0.5 * t_max
                Call FormatConditionShmooColorMap(.Offset(, 7), clct_color_map)
                .Offset(, 8).Value = "[" & Round(0.5 * t_max, 4) & "]"
                
                .Offset(, 14).Value = 1 * t_max
                Call FormatConditionShmooColorMap(.Offset(, 14), clct_color_map)
                .Offset(, 15).Value = "[" & Round(1 * t_max, 4) & "]"
                .Offset(, 21).Value = "(gradient)"
                
            Else
                For i = 1 To clct_color_map.Count
                    t = clct_color_map.Item(i)
                    .Offset(, 0 + (i - 1) * 7).Value = t(0)
                    Call FormatConditionShmooColorMap(.Offset(, 0 + (i - 1) * 7), clct_color_map)
                    .Offset(, 1 + (i - 1) * 7).Value = IIf(i <> clct_color_map.Count, "", "]") & Round(t(0), 4) & "" & IIf(i <> clct_color_map.Count, "~", "")
                Next
            End If
        End With
        .Font.Size = 12
    End With

End Sub

Sub setShmooPlotRowHeightAndColumnWidth(rng_shmoo_plot As Range)
    
    Dim nX, nY
    Dim factor_size
    nX = rng_shmoo_plot.Columns.Count
    nY = rng_shmoo_plot.Rows.Count
    With rng_shmoo_plot
        factor_size = 1
        factor_size = WorksheetFunction.Max(factor_size, nY / 60)
        factor_size = WorksheetFunction.Max(factor_size, nX / 80)
    End With
    
    ' set the row height and column width in the final because font size may change the row height.
    Dim unit_column_width
    unit_column_width = 1.215
    rng_shmoo_plot.Resize(, rng_shmoo_plot.Columns.Count + 5).ColumnWidth = unit_column_width
    With rng_shmoo_plot
        .Offset(-3, -4).Resize(.Rows.Count + 3 + 7, .Columns.Count + 4 + 6).RowHeight = 12
        If factor_size > 1 Then
            .Resize(1).Offset(-2).EntireRow.AutoFit
        End If
    End With
    rng_shmoo_plot.Resize(1).Offset(nY + 3).Resize(3).EntireRow.AutoFit
    With rng_shmoo_plot.Resize(1, 1)
        .Offset(, -1).ColumnWidth = unit_column_width * 2
        .Offset(, -2).ColumnWidth = unit_column_width * 10
        .Offset(, -3).ColumnWidth = unit_column_width * 3
        .Offset(, -4).ColumnWidth = 8.43
        .Offset(nY).RowHeight = 10.5
    End With

End Sub

Sub mergeGrains(rng_shmoo_plot As Range, Optional gsx = 1, Optional gsy = 1)

    Dim nX, nY
    Dim sht As Worksheet
    Dim r0, c0
    Dim old_display_alert
    
    With rng_shmoo_plot
        nX = .Columns.Count / gsx
        nY = .Rows.Count / gsy
        Set sht = .Parent
        r0 = .Row
        c0 = .Column
    End With
    
    old_display_alert = Application.DisplayAlerts
    Application.DisplayAlerts = False
    With rng_shmoo_plot.Resize(gsy, gsx)
        .MergeCells = True
        .Copy
    End With
    Application.DisplayAlerts = old_display_alert
    
    If nY > 1 Then
        sht.Cells(r0 + gsy, c0).Resize((nY - 1) * gsy, gsx).PasteSpecial xlPasteFormats
    End If
    
    If nX > 1 Then
        rng_shmoo_plot.Resize(, gsx).Copy
        sht.Cells(r0, c0 + gsx).Resize(nY * gsy, (nX - 1) * gsx).PasteSpecial xlPasteFormats
    End If

    
End Sub
